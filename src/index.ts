import 'reflect-metadata'
import createSchema from './schema'

createSchema().catch((e) => {
  console.error(e)
})
