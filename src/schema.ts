import { GraphQLSchema } from 'graphql'
import * as path from 'path'
import { buildSchema } from 'type-graphql'

export default function createSchema (): Promise<GraphQLSchema> {
  return buildSchema({
    resolvers: [path.join(__dirname, './test.ts')]
  })
}
