/*tslint:disable:max-classes-per-file*/

import GraphQLJSON from 'graphql-type-json'
import { Field, ObjectType, Query, Resolver } from 'type-graphql'

@ObjectType()
class Page {
  @Field(() => [GraphQLJSON])
  public blocks: string[]
  @Field(() => String)
  public id: string
}

@Resolver()
export abstract class PageResolver {
  @Query(() => Page)
  public getPage (): Page {
    return new Page()
  }
}
